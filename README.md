<h4 style="text-align: center">Mô hình Microservice BookStore</h4>
<img src = "https://i.imgur.com/n8ji9Fe.jpeg"/>
<br/>
<p>
Tôi đã xây dựng một hệ thống theo kiến ​​trúc vi dịch vụ mạnh mẽ có tính năng xác thực người dùng, đăng ký, hoạt động CRUD cho sách, quản lý giỏ hàng và đặt hàng, tất cả đều được hỗ trợ bởi MySQL. Hiệu suất được nâng cao bằng cách sử dụng Redis để lưu vào bộ nhớ đệm. JWT đảm bảo xác thực và ủy quyền an toàn. Các dịch vụ giao tiếp liền mạch thông qua Dịch vụ Discovery. API Gateway bảo mật các điểm cuối, triển khai cân bằng tải để đạt hiệu quả và thử lại các yêu cầu về khả năng phục hồi. Bộ giới hạn tốc độ ở cả phía máy khách và máy chủ sẽ tối ưu hóa hiệu suất và bảo vệ khỏi các cuộc tấn công DDoS. Toàn bộ hệ thống chạy trong các vùng chứa Docker do Docker-Compose quản lý, trong đó Jenkins tự động hóa quy trình CI/CD được tích hợp với GitLab thông qua webhook.</p>
<br/>
<h4>Call-api-service</h4>
<img src="https://i.imgur.com/nwvAr0q.jpeg">
<h4>Discovery localhost</h4>
<img src="https://i.imgur.com/BRHoHY3.png">
<h4>Discovery - Docker</h4>
<img src="https://i.imgur.com/5Gc2L55.png">
<h4>Container run</h4>
<img src="https://i.imgur.com/94m5cO9.png">
<h4>Jenkins build maven file, build docker file, push image docker hub</h4>
<img src="https://i.imgur.com/RMis8iR.png">
<h4>Jenkins auto push image docker hub khi có thay đổi code trên gitlab</h4>
<img src="https://i.imgur.com/6tAMeoz.png">
<h4>Sau khi push len docker hub thì nó tự động pull images xuống</h4>
<img src="https://i.imgur.com/AjEbEqF.png">
<h4>Add webhook cho gitlab khi có thay đổi code thì nó tự động clone code cho mình</h4>
<img src="https://i.imgur.com/FK6z7c9.png">
